- Gartner Magic Quadrant - AST : https://www.gartner.com/doc/reprints?id=1-267XOMMA&ct=210601&st=sb<br>
--> 변경된 링크 : https://www.gartner.com/reprints/?id=1-29SJEK2V&ct=220421&st=sb

- Forrester - SAST : https://reprints2.forrester.com/#/assets/2/982/RES162015/report

- Openchain KWG (Korea Working Group) : https://openchain-project.github.io/OpenChain-KWG/<br>
한국 주요회사에서 Open Source관리하는 팀 사람들끼리 서로 정보를 교환하는 곳

- OWASP : https://owasp.org/<br>
보안취약점(security vulnerability)과 관련해서 방지 작업을 하는 프로젝트로 내부에 여러 서브 프로젝트가 존재<br>

- OWASP Top 10 : https://owasp.org/www-project-top-ten/<br>
3~4년마다 한 번씩 기간동안 제일 많이 발견된 취약점 10개를 리스트해서 리포트로 제공하는 OWASP내 프로젝트 중 하나<br>
OWASP Top 10에 있는 보안취약점에 대해 대략 이해할 필요 있음<br>
Injection - SQLI / XSS / CSRF/SSRF 등은 많이 사용되는 해킹 방법이니 어떤 식으로 해킹하는 지 어떻게 막는 지에 대해 간단하게 이해<br>
--> 2021년 OWASP Top 10에서는 기존에 계속 1등하던 Injection이 3등으로 밀리고, 1등/2등이 새로 올라옴: 웹 애플리케이션 보안에 중점적으로 볼 부분이 바뀌었음을 시사

- Python 으로 script를 짜는 경우가 많으니, Python으로 간단하게 프로그램 작성 연습 (언어 자체가 어렵지 않아서 몇 개 정도만 해보면 금방 이해 됨)

- MMIV Loader에서 사용한 static analysis 툴 이름은 CANTATA https://www.qa-systems.com/tools/cantata/<br>
유닛테스트나 단위테스트에 치중된 툴이라 프로그램 전체나 함수 사이를 이해하고 분석하는데는 한계가 있어 보임. SAST나 static analysis에 대한 경험으로 얘기할 수 있음

- Synopsys SIG 기본 웹사이트 https://www.synopsys.com/software-integrity.html 에서 생각보다 많은 읽을꺼리를 찾을 수 있습니다.<br>
예를 들어, Black Duck 관련해서는 아래와 같은 페이지들이 있습니다.<br>
https://www.synopsys.com/software-integrity/solutions/open-source-security.html<br>
https://www.synopsys.com/software-integrity/security-testing/software-composition-analysis.html<br>
https://www.synopsys.com/software-integrity/solutions/application-security-testing.html<br>
그리고, https://www.synopsys.com/glossary.html 에서 원하는 주제에 대해 좀 더 상세한 정보를 쉽게 볼 수 있습니다. 이 페이지는 EDA쪽 주제도 같이 있으니 주의하세요.
<br>

- Static Analysis vs. Dynamic Analysis : Coverity-Basic-StaticAnalysis.pptx 파일 참고. 이 파일은 정적분석이 좋다는 입장에서 기술한 것임에 유의.<br>
Web Application에서 static analysis vs. dynamic analysis는 https://www.veracode.com/blog/secure-development/static-testing-vs-dynamic-testing 를 참고<br>

- Package manager 혹은 project file<br><br>
아래는 Coverity에서 지원하는 project file<br>
-- JavaScript - npm, yarn : package.json<br>
-- JavaScript - bower : bower.json<br>
-- Java - Maven : pom.xml<br>
-- Java - Gradle : build.gradle<br>
-- .NET - C# : .sln .csproject<br><br>
Black Duck은 지원하는 package manager가 더 많음. https://synopsys.atlassian.net/wiki/spaces/INTDOCS/pages/631275892/Detectors 참고<br>

- Integration 관련 많이 사용되는 툴/기술 들<br>
-- CI : Jenkins, Bamboo <br>
-- CD : Docker (Orchestration tool: Kubernetes, Docker Swarm, OpenShift) <br>
-- Issue Tacking : JIRA, Bugzilla <br>
-- Sign-in : LDAP, SSO (Kerberos, Okta, Keycloak), SSO관련 기술 (SAML, OpenID) <br>
-- Cloud : AWS, GCP, Azure <br>
-- Version Management System : Git, Github, Gitlab, Bitbucket (세 가지 모두 Git 기반임), Perforce, Clearcase, CVS, SVN, TFS (Team Foundation Server) <br>
-- Web API : SOAP, RESTful <br>

- 알아놓으면 도움이 되는 용어 <br>
-- HA (High Availability) <br>
-- DR (Disaster Recovery) <br>
-- Load Balance <br>
-- Microservices Architecture <br>

- 오픈소스 관련<br>
-- 오픈소스 라이선스 가이드 (첨부) <br>
결론적으로는 GPL말고는 다 써도 된다는 건데, 좀 구체적인 라이선스 별 정보가 궁금할 때 참조<br>
-- 금감원_금보원오픈소스 소프트웨어 활용·관리 안내서2022.pdf (첨부) <br>
금융 쪽에서 오픈소스 관리를 어떻게 할 지 금감원에 나온 가이드 입니다. 일반적인 오픈소스 관리 방법을 기술해 놓은 문서라 이해하기 좋습니다. <br>

- WebGoat : https://owasp.org/www-project-webgoat/<br>
OWASP내 프로젝트 중 하나입니다. Java베이스로 일부러 보안 취약점이 많은 웹어플리케이션을 만들어서, 각 페이지에서 어떻게 해킹을 하는 지 스스로 배우는 목적입니다. 구글 검색해 보면 WebGoat의 각 페이지를 어떻게 해킹하는지 답들도 찾을 수 있습니다. 코드는 https://github.com/WebGoat/WebGoat 에서 받을 수 있고 docker나 다른 방식으로 쉽게 웹 어플리케이션을 띄울 수 있으니 한 번 해보면 바로 감을 잡을 수 있습니다. 이게 뭔지 정도 이해하면 되고, security vulnerability를 이용한 공격이 어떻게 되는 지, 어떻게 방어하는 지 등을 배우는 기초로 좋다고 합니다.<br>

- NodeGoat (Node.js Goat) : https://owasp.org/www-project-node.js-goat/ https://github.com/OWASP/NodeGoat<br>
WebGoat의 아류 프로젝트. Node.JS 베이스입니다. 시작은 좋은데, WebGoat처럼 쉽게 띄우기도 어렵고 (DB를 따로 설치해야 함) 규모도 WebGoat보다 작습니다.<br>

- https://owasp.org/www-project-vulnerable-web-applications-directory/ 보안 취약점을 다루다 보면 어려운 점 중에 하나가, 실제 예제를 구하기 어렵다는 점 입니다. 실제 운용하는 웹 페이지에 보안 구멍을 뚫어 놓을 수는 없으니까요. 그래서 OWASP에서 WebGoat, NodeGoat처럼 프로젝트로 보안에 취약한 웹 어플리케이션을 언어별로 목적별로 만들어서 이 페이지에서 관리(?)하고 있습니다. 필요한 언어의 웹 어플리케이션이 필요할 때 이 페이지에서 찾아보는 것도 방법입니다.<br>

- NVD : https://nvd.nist.gov/<br>
미국 정부에서 관리하는 software vulnerability DB 사이트 입니다. 발견된 보안 취약점을 DB로 등록해서 관리합니다. Black Duck과 경쟁사 툴 들이 모두 이 DB에서, 발견한 component에 어떤 취약점이 있는 지 확인합니다. NVD에서는 발견된 보안 취약점 CVE(Common Vulnerability Enumeration) 번호로 관리합니다. 예를 들면, https://nvd.nist.gov/vuln/detail/cve-2019-10744 를 참고하십시오. 그리고 각 CVE별로 CVSS (https://nvd.nist.gov/vuln-metrics/cvss) 로 점수를 매겨서 위험도를 표시합니다.<br>

- CWE : https://cwe.mitre.org/<br>
미국 정부의 지원을 받는 MITRE라는 회사(연구기관?)에서 소프트웨어+하드웨어에 걸친 약점(weakness)을 번호로 관리하는 사이트 입니다. 예를 들면, https://cwe.mitre.org/data/definitions/248.html 를 참고하십시오.<br>
일부 cwe는 자기 자신은 특별한 내용이 없이 하위cwe만 있는 계층 조를 가집니다. (예, cwe-658, cwe-659)<br>
CVE와 CWE가 헷갈리니 유의하세요. Synopsys 툴 관점에서 보면, CVE는 Black Duck에서 관심이 있고, CWE는 Coverity에서 관심이 있습니다.<br>

- 2021_오픈소스 시큐리티_리스크분석보고서_Korea.pdf (첨부)<br>
시높시스에서 발행한 open source security risk report 번역본. 참고삼아 읽어볼 만 합니다.<br>

- IDE 경험 - Synopsys 툴에서 플러그인을 지원하는 각종 IDE들, 할 수 있으면 한 번씩 설치해서 간단한 프로젝트를 올려서 돌려보면 좋습니다.<br>
-- Eclipse <br>
-- Visual Studio <br>
-- IntelliJ IDEA : 이 외에도 JetBrain의 다른 IDE들도 지원하는 툴이 있으니 이름은 알아두십시오 <br>
-- Android Studio <br>
-- VS Code : IDE는 아니지만 워낙 광범위하게 쓰여서 plug-in을 지원합니다 <br>

- 언어 : C/C++ > Java > JavaScript == Kotlin == GO lang > C# > Python == PHP == Objective-C 정도까지는 대략적인 특징, 쓰이는 곳 등에 대해 알아두면 좋습니다.<br>
CUDA도 한 번 정리해 놓으시면 도움될 듯 합니다. 요즘 비트코인 채굴은 아마 다 CUDA로 짠 프로그램일 겁니다. ㅎㅎ<br>
clang도 뭔지 한 번 읽어두세요. 간단하게는 gcc대신 쓰는 C/C++ 컴파일러입니다. 최근에 많이 씁니다. LLVM clang, Apple clang<br>

- 프레임워크 : Java, JavaScript 위주로 많이 쓰는 프레임워크에 대해 정리해 놓으면 좋습니다. Spring(Java), Struts(Java), Node.JS(JavaScript), ASP.NET(C#), Angular.JS(JavaScript) 등...<br>

- 빌드시스템 : Yocto, Buildroot, Make, Ninja, Soong, Ant, Maven, Gradle, Xcodebuild 등 주요 빌드시스템에 대해 정리해서 알고 있을 필요가 있습니다. 실무에서 경험도 있으면 정리해 놓으면 좋습니다.<br>

- 데이터베이스 : PostgreSQL (시높시스 제품은 거의 이 DB), mySQL, MongoDB 등에 대해 정리 + 실무경험정리<br>

- Proxy / Reverse proxy<br>

- 주요 네트웍 포트 사용처 : https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers 80, 8008, 8080, 443, 8443, 5432 정도만 알고 있으면 될 거 같습니다.<br>

- FlexNet 라이선스 동작 방식 : https://en.wikipedia.org/wiki/FlexNet_Publisher 참고하시고, 그냥 키가 중앙 서버에 있고 동작할 때 키를 가져와서 동작하고 다시 키를 반납하는 방식이라는 정도만 아시면 됩니다. <br>

- SDLC, waterfall, agile 등 소프트웨어 개발 주기, 방법론에 대한 정리도 필요합니다<br>

